﻿using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tuner
{
    public interface ITuner
    {
    }

    public interface ITuner<T> : ITuner where T : ITunerSubject
    {
        bool IsInterested(IEnumerable<T> source);

        IEnumerable<T> Tune(IEnumerable<T> source);
    }

    public interface ITunerSubject
    {
    }

    public class DemoTunerSubject : IHasInt, IHasString
    {
        public int IntProperty { get; set; }
        public string StringProperty { get; set; }
    }

    public interface IHasInt : ITunerSubject
    {
        int IntProperty { get; set; }
    }

    public interface IHasString : ITunerSubject
    {
        string StringProperty { get; set; }
    }

    public class PipeLine
    {
        private readonly List<ITuner> _tuners = new List<ITuner>();

        public void RegisterTunner<T>(ITuner<T> tuner) where T : ITunerSubject
        {
            _tuners.Add(tuner);
        }

        public IEnumerable<T> Tune<T>(IEnumerable<T> source) where T : ITunerSubject
        {
            var result = source;
            
            _tuners.OfType<ITuner<T>>()
                .Where(t => t.IsInterested(source))
                .ToList()
                .ForEach(t => result = t.Tune(source));

            return result;
        } 
    }

    public class SetInitialValueTuner: ITuner<IHasInt>
    {
        public bool IsInterested(IEnumerable<IHasInt> source)
        {
            return true;
        }

        public IEnumerable<IHasInt> Tune(IEnumerable<IHasInt> source)
        {
            foreach (var item in source)
            {
                if (item.IntProperty == 0)
                {
                    item.IntProperty = 42;
                }
            }

            return source;
        }
    }

    public class Add7ToIntTuner : ITuner<IHasInt>
    {
        public bool IsInterested(IEnumerable<IHasInt> source)
        {
            return true;
        }

        public IEnumerable<IHasInt> Tune(IEnumerable<IHasInt> source)
        {
            foreach (var item in source)
            {
                item.IntProperty += 7;
            }

            return source;
        }
    }

    public class Demo
    {
        [Fact]
        public void Show()
        {
            var pipeline = new PipeLine();

            pipeline.RegisterTunner(new SetInitialValueTuner());
            pipeline.RegisterTunner(new Add7ToIntTuner());

            var source = new List<DemoTunerSubject>
            {
                new DemoTunerSubject(),
                new DemoTunerSubject
                {
                    IntProperty = 12
                }
            };

            var output = pipeline.Tune(source as IEnumerable<IHasInt>);
//            var output = pipeline.Tune(source as IEnumerable<IHasString>);
        }
    }
}
